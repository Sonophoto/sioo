## SIOO is an acronym for State Input Operator Output

SiOO is an embeddable cognitive architecture based on Soar 8.5.2. It is being enhanced with a *nix style command shell, an embeddable footprint, and development tools and modifications to the actual cognitive architecture.

***
#### Sponsored By:

SiOO has been made possible by the generous support of Module Master LLC. `http://modulemaster.com/rebuilds/about-us/` Module Master supports the principles of Liberated and Open Source and makes all changes to the underlying system available under the GPLv3 License: `https://gnu.org/licenses/quick-guide-gplv3.html`.

***

#### This code requires the GNU Compiler Collection (GCC)
#### This code will probably only run on GNU/Linux.

***

**This is beta code and is just now coming together.**

This code is NOT for beginners and is intended for experts with the C language, gcc, *nix tools, production systems, the PSCM, FSMs, and Knowledge/Domain Modeling.

There is no support other than the source code. Debugging support is primitive, There is no test suite...

**We haven't scared you off yet? Read the step-by-step how-to on the wiki Home page**

https://github.com/Sonophoto/sioo/wiki
